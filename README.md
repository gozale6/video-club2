# Readme de la Aplicación del Club de Video

Este es el archivo readme para la aplicación del Video club. Esta aplicación está diseñada para gestionar la base de datos de un club de video y manejar diversas operaciones relacionadas con películas, actores, directores, miembros, copias y reservas. A continuación, encontrarás una descripción general de la estructura de la aplicación, sus principales componentes y cómo configurarla.

## Tabla de Contenidos
1. [Descripción General de la Aplicación](#descripción-general-de-la-aplicación)
2. [Estructura de la Aplicación](#estructura-de-la-aplicación)
3. [Instalación](#instalación)
4. [Configuración de la Base de Datos](#configuración-de-la-base-de-datos)
5. [Modelos y Asociaciones](#modelos-y-asociaciones)
6. [Ejecución de la Aplicación](#ejecución-de-la-aplicación)

## Descripción General de la Aplicación

La aplicación del Video club está construida utilizando Node.js y Express.js y utiliza una base de datos MySQL a través de Sequelize ORM. Ofrece funcionalidades para gestionar películas, directores, géneros, actores, miembros, copias y reservas. La aplicación está estructurada para manejar diferentes rutas y métodos HTTP para interactuar con la base de datos.

## Estructura de la Aplicación

La aplicación consta de dos archivos principales:

### app.js
`app.js` es el archivo principal de la aplicación responsable de configurar el servidor Express.js, definir rutas y manejar las solicitudes HTTP. Aquí tienes una descripción general de lo que hace:

- Requiere los paquetes y módulos necesarios.
- Configura la aplicación Express.
- Configura el motor de vistas y sirve archivos estáticos.
- Define varias rutas para diferentes recursos (por ejemplo, películas, actores, miembros).
- Maneja las rutas y la representación de errores.

### db.js
`db.js` es responsable de configurar la base de datos MySQL utilizando Sequelize ORM. Define modelos para varias tablas de la base de datos y establece asociaciones entre ellos. Aquí tienes una descripción general:

- Requiere Sequelize y los modelos necesarios para directores, géneros, películas, actores, miembros, copias y reservas.
- Configura la conexión a la base de datos con el nombre de la base de datos, usuario, contraseña y host.
- Define modelos Sequelize para cada tabla de la base de datos.
- Establece asociaciones entre los modelos, reflejando relaciones como "una película pertenece a un director".
- Sincroniza el esquema de la base de datos y registra un mensaje que indica la sincronización exitosa.

## Instalación

Para configurar y ejecutar la aplicación del Club de Video, sigue estos pasos:

1. Clona este repositorio en tu máquina local:

   ```bash
   git clone <URL-del-repositorio>
   ```

2. Navega hasta el directorio del proyecto:

   ```bash
   cd video-club-app
   ```

3. Instala las dependencias requeridas:

   ```bash
   npm install
   ```

## Configuración de la Base de Datos

Antes de ejecutar la aplicación, debes configurar la conexión a la base de datos. En el archivo `db.js`, encontrarás las siguientes líneas donde puedes especificar los detalles de tu base de datos:

```javascript
const sequelize = new Sequelize('video-club', 'root', 'abcd1234', {
    host: '127.0.0.1',
    dialect: 'mysql'
});
```

- Reemplaza `'video-club'` con el nombre deseado de tu base de datos.
- Actualiza `'root'` y `'abcd1234'` con tu nombre de usuario y contraseña de MySQL.
- Asegúrate de que el `host` y el `dialect` estén configurados correctamente para tu entorno de MySQL.

## Modelos y Asociaciones

Los modelos y asociaciones de la aplicación ya están definidos en `db.js`. Puedes personalizar aún más estos modelos y asociaciones para satisfacer tus requisitos específicos.

## Ejecución de la Aplicación

Una vez que hayas configurado la base de datos y instalado las dependencias, puedes ejecutar la aplicación del Club de Video. Utiliza el siguiente comando:

```bash
npm start
```

La aplicación se iniciará y estará disponible en tu navegador web en `http://localhost:3000`.
